# 元寶微信接口文檔

# 文件目的
  - 本文檔介紹平台接入元寶微信接口、接口參數規範。


# 傳輸規範
  - 使用TCP/IP作為傳輸層協議
  - 使用HTTP作為應用層協議
  - 傳遞參數格式皆為json(須使用Content-type規範header)
  

# MD5 規範
  - 將所有接口請求參數(sign除外)以param-value形式串接,並以param的開頭採ASCII由小至大排序並在最後押上key=MD5key(元寶系統發放)
  - MD5簽名採用utf-8編碼,將結果轉字串取32位小寫作為sign帶入請求參數
  - 簽名原串範例：param1=value1&param2=value2&param3=value3&key=123456
  
  
# 元寶系統接口清單
  - <a href="#user-info-取得使用者的微信資訊">user-info取得使用者的微信資訊</a>
# 廠商系統接口清單  
  - <a href="#notifyUrl-異步通知參數規範">notifyUrl-異步通知參數規範</a>
  - <a href="#redirectUrl-跳轉畫面">redirectUrl-跳轉畫面</a>
  
  
## user-info-取得使用者的微信資訊
### Reuqest
- Method: **GET**
- URL: ```/weixin/user-info/mobile```
- queryString:
```
spId=PL0001&requestId=ORDER201810161917&notifyUrl=http://www.google.com&redirectUrl=http://www.google.com&memo=memo&sign=c08d3e4cb641fbbdc5b06b448dd22395
```
### Response
```
	頁面跳轉(將帶著使用者參數以表單方式傳遞(參數內容與異步同))
```

***

## notifyUrl-異步通知參數規範
### Reuqest
- Method: **POST**
- URL: ```平台提供之notifyUrl位置```
- Headers： Content-Type:application/json
- Body:
```
{
	"requestId": "ORDER201810161917",
	"weixinUserId": "0x55asdasd5d1d",
	"nickName": "微信使用者",
	"headUrl": "0.png",
	"memo": "",	
	"sign": "c08d3e4cb641fbbdc5b06b448dd22395"
}
```
### Response
- Body
```
{
	"retCode":"0"
}
```

***

## redirectUrl-跳轉畫面
### Reuqest
- Method: **POST**
- URL: ```平台提供之redirectUrl位置```
- queryString:
```
requestId=ORDER201810161917&weixinUserId=0x55asdasd5d1d&nickName=微信使用者&headUrl=0.png&memo=memo&sign=c08d3e4cb641fbbdc5b06b448dd22395
```


# 參數說明
|參數名稱|參數格式|參數說明|是否必填|
|:--|:--|:--|:--|
|spId|String(32)|平台ID(由元寶系統發配)|是|
|requestId|String(32)|請求ID，用於配對使用者資訊|是|
|memo|String(32)|自定義字段(原值返還至notifyUrl、redirectUrl)|否|
|notifyUrl|String(32)||否|
|redirectUrl|String(32)||是|
|sign|String(32)|簽名|是|
|weixinUserId|String(32)|使用者唯一識別|是|


# 待更新項目
 - 須補全局錯誤碼
 
 
# 更新日誌
 - 2018/10/12	創建文檔	by Engine
 
